---
output:
  flexdashboard::flex_dashboard:
    orientation: rows
    theme: lumen
title: "Dashboard with humanitarian icons"  
---


```{r setup, include=FALSE}
library(ggplot2)
library(scales)
library(plotly)
library(humicons)
library(readr)
library(dplyr)
library(forcats)
library(hrbrthemes)
library(htmltools)
library(flexdashboard)

diverging_bar <- read_csv("data/diverging-bar.csv") %>%
    mutate(`Profit Ratio` = parse_number(`Profit Ratio`) / 100,
           `Sub-Category` = fct_reorder(`Sub-Category`, `Profit Ratio`))
                                                       
n <- nrow(diverging_bar)
col <- colorRampPalette(c("#0F7B9E", "#DCB86D"))(n)
names(col) <- diverging_bar$`Sub-Category`

surplus_line <- read_csv("data/surplus-line.csv")
surplus_line$`Month of Order Date` <- as.Date(surplus_line$`Month of Order Date`)

df <- surplus_line %>%
    mutate(positive = Diff > 0,
           Diff_pos = ifelse(positive, Diff, 0),
           Diff_neg = ifelse(!positive, Diff, 0)) 

br <- seq.Date(as.Date("2015-01-01"), as.Date("2018-10-01"), by = "6 months")
K <- unit_format(unit = "K", scale = 1e-3)
M <- unit_format(unit = "M", scale = 1e-6, accuracy = .1)
```

`r humicons::hi("Cyclone", size = 2, color = "steelblue")`
=======================================================================

Row
-----------------------------------------------------------------------

### Tsunami

```{r}
valueBox(tags$strong(2), humicons::hi("Tsunami", size = 3), icon = humicons::hi("Tsunami"))
```

### Drought

```{r}
valueBox(tags$strong(10), humicons::hi("Drought", size = 3), icon = humicons::hi("Drought"), color = "warning")
```


Row
-----------------------------------------------------------------------

### Bar chart

```{r}
diverging_bar %>%
    ggplot() +
    geom_col(aes(x = `Sub-Category`, y = `Profit Ratio`, fill = `Sub-Category`)) +
    coord_flip() +
    labs(x = "", y = "") +
    scale_fill_manual(values = col) +
    scale_y_percent() +
    theme_ipsum_rc(grid = "X") +
    theme(legend.position = "none") -> diverging_bar_plot
ggplotly(diverging_bar_plot)
```


### Area chart

```{r}
ggplot(df) +
    geom_area(aes(x = `Month of Order Date`, y = Diff_pos), fill = "#0F7B9E", alpha = 0.5) +
    geom_line(aes(x = `Month of Order Date`, y = Diff_pos), colour = "#0F7B9E") +
    geom_area(aes(x = `Month of Order Date`, y = Diff_neg), fill = "#DCB86D", alpha = 0.5) +
    geom_line(aes(x = `Month of Order Date`, y = Diff_neg), colour = "#DCB86D") +
    theme_ipsum_rc(grid = "X") +
    labs(x = "", y = "") +
    scale_x_date(date_labels = "%b %Y", breaks = br) +
    scale_y_continuous(labels = K, limits = c(-50000, 50000), breaks = c(-50000, 0, 50000)) +
    theme(legend.position = "none") -> area_plot
ggplotly(area_plot)
```


